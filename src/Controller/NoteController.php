<?php

/*
 * This file is part of the project symfony-sandbox.
 */

namespace App\Controller;

use App\Form\NoteType;
use App\Repository\NoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NoteController extends AbstractController
{
    /**
     * @Route("/", name="note")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(NoteRepository $noteRepository)
    {
        $notes = $noteRepository->findAll();

        return $this->render('note/index.html.twig', [
            'notes' => $notes,
        ]);
    }

    /**
     * @Route("/create", name="note_create")

     *
     * @param NoteRepository $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(NoteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($form->getData());
            $entityManager->flush();

            return $this->redirectToRoute('note');
        }

        return $this->render('note/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
